# REUSE

[![Build Status](https://drone.fsfe.org/api/badges/FSFE/reuse-web/status.svg)](https://drone.fsfe.org/FSFE/reuse-web)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/fsfe/reuse-web)


We're working to make managing copyrights and licenses in free and open
source software easier. This is the website for a guide of best practices,
meant to demonstrate how 
to add copyright and license information to a project in ways which allow
for more automation.

## Install

There's no installation here, but you may try running `hugo` in the
`site/` directory to generate the website when testing locally.
Typically, we let our Drone CI build and deploy the website for us.

## Usage

Go to https://reuse.software/ and enjoy :-) If your project follows the reuse
guidelines, we encourage you to show that in your `README.md` and similar! Just
copy this badge into your readme:

`
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/fsfe/reuse-web)
`

## Contribute

We'd love to get feedback on these practices, ideally in the form
of pull requests which we can discuss around. To be able to contribute
in this way, you need an account on `git.fsfe.org`, which you can
get by going to our [account creation page](https://fsfe.org/fellowship/ams/index.php?ams=register). This will sign you up for a volunteer account with the FSFE.

Once you've registered, your account needs to be activated. Just shoot a mail to <contact@fsfe.org> or directly to <jonas@fsfe.org> saying you've registered and would like to be activated. As soon as your account is activated, you can set a username and proceed to login to `git.fsfe.org`.

We also accept and appreciate feedback by creating issues in the project
(requires the same account creation), or by sending e-mail to, again,
<contact@fsfe.org> or <jonas@fsfe.org>.

## License

The theme used for this website is based on [Hugo Sp-Minimal](https://github.com/eueung/hugo-sp-minimal) which in itself is based on [Minimal](https://github.com/orderedlist/minimal), both of which are licensed under
the [MIT license](https://github.com/eueung/hugo-sp-minimal/blob/master/LICENSE.md).

The content of the website, the best practices, are licensed under [Creative Commons Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0).
